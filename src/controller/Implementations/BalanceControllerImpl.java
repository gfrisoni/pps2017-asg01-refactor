package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JTable;

import controller.Interfaces.BalanceController;
import model.Implementations.MovementImpl;
import view.Interfaces.BalanceView;
import view.Interfaces.MoreFunctionsView;

/**
 * controller che gestisce la balance view
 */
public class BalanceControllerImpl implements BalanceController {

	private static final String MOVEMENT_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
	
	private static BalanceView balanceView;
	
	private SimpleDateFormat dateFormat;

	/**
	 * costruttore
	 * 
	 * @param balanceView
	 *            la view
	 */
	BalanceControllerImpl(BalanceView balanceView) {
		BalanceControllerImpl.balanceView = balanceView;
		balanceView.addBackListener(new BackListener());
		showBalance();
		this.dateFormat = new SimpleDateFormat(MOVEMENT_DATE_FORMAT);
	}

	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			balanceView.dispose();
			new MoreFunctionsControllerImpl(new MoreFunctionsView(balanceView.getRank()));
		}
	}

	public void showBalance() {
		int balance = 0;
		// salvo tutti i movimenti passati in una lista
		final List<MovementImpl> list = FileUtilities.readObjects(ApplicationFile.MOVEMENTS);
		// li ciclo e calcolo il bilancio
		for (MovementImpl m : list) {
			if (m.isPositive()) {
				balance += m.getAmount();
			} else {
				balance -= m.getAmount();
			}
		}
		// stampo il bilancio
		balanceView.setLabel(String.valueOf(balance));
		// creo una tabella con tutti i movimenti
		final String[] vet = { "+ : -", "amount", "desc", "data" };
		final String[][] mat = new String[list.size()][vet.length];
		for (int i = 0; i < list.size(); i++) {
			mat[i][0] = list.get(i).isPositive() ? "+" : "-";
			mat[i][1] = String.valueOf(list.get(i).getAmount());
			mat[i][2] = list.get(i).getDescription();
			mat[i][3] = this.dateFormat.format(list.get(i).getDate());;
		}
		final JTable table = new JTable(mat, vet);
		balanceView.createTable(table);
	}
	
}
