package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.JTable;

import controller.Interfaces.ViewCellsController;
import model.Implementations.CellImpl;
import model.Interfaces.Cell;
import model.Interfaces.Floor;
import model.Interfaces.Prison;
import view.Interfaces.MoreFunctionsView;
import view.Interfaces.ViewCellsView;

/**
 * controller della view cells view
 */
public class ViewCellsControllerImpl implements ViewCellsController {

	static ViewCellsView viewCellsView;

	/**
	 * costruttore
	 * 
	 * @param viewCellsView
	 *            la view
	 */
	public ViewCellsControllerImpl(ViewCellsView viewCellsView) {
		ViewCellsControllerImpl.viewCellsView = viewCellsView;
		viewCellsView.addBackListener(new BackListener());
		viewCellsView.createTable(getTable());
	}

	/**
	 * listener che apre la view precedente
	 */
	public static class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			viewCellsView.dispose();
			new MoreFunctionsControllerImpl(new MoreFunctionsView(viewCellsView.getRank()));
		}
	}

	public JTable getTable() {
		final Optional<Prison> prison = FileUtilities.readObject(ApplicationFile.PRISON);
		List<Cell> cells = new ArrayList<>();
		if (prison.isPresent()) {
            cells = prison.get().getFloors()
                    .stream()
                    .map(Floor::getCells)
                    .flatMap(List::stream)
                    .collect(Collectors.toList());
        }
		final String[] vet = { "ID Cella", "Prigionieri correnti", "Capacità max" };
		// salvo i dati delle celle in una matrice
		final String[][] mat = new String[cells.size()][vet.length];
		for (int i = 0; i < cells.size(); i++) {
			mat[i][0] = String.valueOf(cells.get(i).getId());
			mat[i][1] = String.valueOf(cells.get(i).getPrisonersCount());
			mat[i][2] = String.valueOf(cells.get(i).getCapacity());
		}
		// creo la tabella passandogli i dati della matrice
		final JTable table = new JTable(mat, vet);
		table.getColumnModel().getColumn(0).setPreferredWidth(20);
		table.getColumnModel().getColumn(1).setPreferredWidth(200);
		return table;
	}
	
}
