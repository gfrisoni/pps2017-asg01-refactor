package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Objects;

import controller.Interfaces.InsertGuardController;
import model.Interfaces.Guard;
import view.Interfaces.InsertGuardView;
import view.Interfaces.SupervisorFunctionsView;

/**
 * controller che gestisce la insert guard view
 */
public class InsertGuardControllerImpl implements InsertGuardController {

	private static InsertGuardView insertGuardView;

	/**
	 * costruttore
	 * 
	 * @param insertGuardView
	 *            la view
	 */
    InsertGuardControllerImpl(InsertGuardView insertGuardView) {
		InsertGuardControllerImpl.insertGuardView = insertGuardView;
		insertGuardView.addBackListener(new BackListener());
		insertGuardView.addInsertListener(new InsertListener());
	}

	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertGuardView.dispose();
			new SupervisorControllerImpl(new SupervisorFunctionsView(insertGuardView.getRank()));
		}
	}

	public void insertGuard() {
		final List<Guard> guards = FileUtilities.readObjects(ApplicationFile.GUARDS_USER_PASS);
		// recupero la guardia inserita nella view
		final Guard g = insertGuardView.getGuard();
		boolean contains = false;
		// controllo che non ci siano errori
		Objects.requireNonNull(guards);
		for (Guard g1 : guards) {
			if (g1.getId() == g.getId()) {
				insertGuardView.displayErrorMessage("ID già usato");
				contains = true;
			}
		}
		if (isSomethingEmpty(g)) {
			insertGuardView.displayErrorMessage("Completa tutti i campi correttamente");
			contains = true;
		}
		if (!contains) {
			// inserisco la guardia e salvo la lista aggiornata
			guards.add(g);
			FileUtilities.writeObjects("res/GuardieUserPass.txt", guards);
			insertGuardView.displayErrorMessage("Guardia inserita");
		}
	}

	/**
	 * listener che si occupa di inserire una guardia
	 */
	public class InsertListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertGuard();
		}
	}

	public boolean isSomethingEmpty(Guard g) {
		return g.getName().equals("") || g.getSurname().equals("")
				|| g.getRank() < 1 || g.getRank() > 3
				|| g.getId() < 0
				|| g.getPassword().length() < 6;
	}
	
}
