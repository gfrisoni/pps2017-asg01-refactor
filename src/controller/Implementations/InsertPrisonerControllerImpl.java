package controller.Implementations;

import controller.Interfaces.InsertPrisonerController;
import model.Implementations.factories.PrisonerFactoryImpl;
import model.Interfaces.*;
import model.Interfaces.factories.PrisonerFactory;
import view.Interfaces.InsertPrisonerView;
import view.Interfaces.MainView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.stream.Collectors;

/**
 * controller della view insertprisoner
 */
public class InsertPrisonerControllerImpl implements InsertPrisonerController {

	private static InsertPrisonerView insertPrisonerView;
	
	private final Set<CrimeType> selectedCrimes;

	/**
	 * costruttore
	 * 
	 * @param insertPrisonerView
	 *            la view
	 */
    InsertPrisonerControllerImpl(InsertPrisonerView insertPrisonerView) {
		InsertPrisonerControllerImpl.insertPrisonerView = insertPrisonerView;
		insertPrisonerView.addInsertPrisonerListener(new InsertPrisonerListener());
		insertPrisonerView.addBackListener(new BackListener());
		insertPrisonerView.addAddCrimeListener(new AddCrimeListener());
		this.selectedCrimes = new HashSet<>();
	}

	public void insertPrisoner() {
        // recupero le liste di prigionieri correnti e storici
		final List<Prisoner> prisoners = FileUtilities.readObjects(ApplicationFile.PRISONERS);
		final List<Prisoner> currentPrisoners = FileUtilities.readObjects(ApplicationFile.CURRENT_PRISONERS);
        // recupero la prigione salvata
		final Optional<Prison> prison = FileUtilities.readObject(ApplicationFile.PRISON);
		prison.ifPresent(p -> {

            // recupero il prigioniero inserito nella view
            final PrisonerFactory prisonerFactory = new PrisonerFactoryImpl();
            final Prisoner prisoner = prisonerFactory.createPrisoner(
                    insertPrisonerView.getPrisonerID1(),
                    insertPrisonerView.getName1(),
                    insertPrisonerView.getSurname1(),
                    insertPrisonerView.getBirth1(),
                    insertPrisonerView.getStart1(),
                    insertPrisonerView.getEnd1(),
                    this.selectedCrimes);

            // controllo che non ci siano errori
            boolean correct = true;
            if (isSomethingEmpty(prisoner)) {
                insertPrisonerView.displayErrorMessage("Completa tutti i campi");
            } else {
                for (Prisoner pr : prisoners) {
                    if (pr.getId() == prisoner.getId()) {
                        insertPrisonerView.displayErrorMessage("ID già usato");
                        correct = false;
                    }
                }
                final Calendar today = Calendar.getInstance();
                today.set(Calendar.HOUR_OF_DAY, 0);
                if (correct) {
                    if (prisoner.getImprisonmentStartDate().after(prisoner.getImprisonmentEndDate())
                            || prisoner.getImprisonmentStartDate().before(today.getTime())
                            || prisoner.getBirthDate().after(today.getTime())) {
                        insertPrisonerView.displayErrorMessage("Correggi le date");
                    } else {
                        // aggiungo nuovo prigioniero in entrambe le liste
                        prisoners.add(prisoner);
                        currentPrisoners.add(prisoner);
                        // controllo che la cella inserita non sia piena
                        final Optional<Cell> cell = p.getFloors()
                                .stream()
                                .map(Floor::getCells)
                                .flatMap(List::stream)
                                .collect(Collectors.toList())
                                .stream()
                                .filter(c -> c.getId() == insertPrisonerView.getCellID())
                                .findFirst();
                        cell.ifPresent(c -> {
                            if (c.getCapacity() == c.getPrisonersCount()) {
                                insertPrisonerView.displayErrorMessage("Prova con un'altra cella");
                            } else {
                                c.addPrisoner(prisoner);
                            }
                        });
                        // salvo la prigione aggiornata
                        FileUtilities.writeObject("res/Prison.txt", prison);
                        // salvo le liste di prigionieri aggiornate
                        FileUtilities.writeObjects("res/Prisoners.txt", prisoners);
                        FileUtilities.writeObjects("res/CurrentPrisoners.txt", currentPrisoners);
                        insertPrisonerView.displayErrorMessage("Prigioniero inserito");
                    }
                }
            }

        });
	}

	/**
	 * listener che si occupa di inserire prigionieri
	 */
	public class InsertPrisonerListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertPrisoner();
		}

	}

	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertPrisonerView.dispose();
			new MainControllerImpl(new MainView(insertPrisonerView.getRank()));
		}

	}

	/**
	 * listener che aggiunge un crimine alla lista dei crimini del prigioniero
	 */
	public class AddCrimeListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			final CrimeType selectedCrime = insertPrisonerView.getSelectedCrime();
			if (selectedCrimes.contains(selectedCrime)) {
				insertPrisonerView.displayErrorMessage("Crimine gi� inserito");
			} else {
				selectedCrimes.add(selectedCrime);
				insertPrisonerView.setCrimeSet(selectedCrimes);
			}
		}
	}

	public boolean isSomethingEmpty(Prisoner p) {
        return p.getName().isEmpty() || p.getSurname().isEmpty() || p.getCrimes().isEmpty();
    }
	
}
