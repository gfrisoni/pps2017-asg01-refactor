package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Objects;

import model.Interfaces.Guard;
import view.Interfaces.LoginView;
import view.Interfaces.MainView;

/**
 * controller della login view
 */
public class LoginControllerImpl {

	private LoginView loginView;

	/**
	 * costruttore
	 * 
	 * @param loginView
	 *            la view
	 */
	public LoginControllerImpl(LoginView loginView) {
		this.loginView = loginView;
		loginView.addLoginListener(new LoginListener());
		loginView.displayErrorMessage("accedere con i profili: \n id:3 , password:qwerty \n id:2 , password:asdasd ");
	}

	/**
	 * listener che si occupa di effettuare il login
	 */
	public class LoginListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {

			final List<Guard> guards = FileUtilities.readObjects(ApplicationFile.GUARDS_USER_PASS);
			boolean isInside = false;

			// controllo che non ci siano errori
			if (loginView.getUsername().isEmpty() || loginView.getPassword().isEmpty()) {
				loginView.displayErrorMessage("Devi inserire username e password");
			} else {
				Objects.requireNonNull(guards);
				for (Guard g : guards) {
					if (loginView.getUsername().equals(String.valueOf(g.getId()))
							&& loginView.getPassword().equals(g.getPassword())) {
						// effettuo il login
						isInside = true;
						loginView.displayErrorMessage("Benvenuto Utente " + loginView.getUsername());
						loginView.dispose();
						new MainControllerImpl(new MainView(g.getRank()));
					}
				}
				if (!isInside) {
					loginView.displayErrorMessage("Combinazione username/password non corretta");
				}
			}
		}

	}
	
}
