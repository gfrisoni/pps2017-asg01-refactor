package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import model.Interfaces.Prisoner;
import model.Interfaces.Visitor;
import view.Interfaces.AddVisitorsView;
import view.Interfaces.MoreFunctionsView;

/**
 * controller della addVisitorsView
 */
public class AddVisitorsControllerImpl {

	static AddVisitorsView visitorsView;

	/**
	 * costruttore
	 * 
	 * @param view
	 *            la view
	 */
	AddVisitorsControllerImpl(AddVisitorsView view) {
		AddVisitorsControllerImpl.visitorsView = view;
		visitorsView.addBackListener(new BackListener());
		visitorsView.addInsertVisitorListener(new InsertListener());
	}

	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			visitorsView.dispose();
			new MoreFunctionsControllerImpl(new MoreFunctionsView(visitorsView.getRank()));
		}
	}

	/**
	 * listener che gestisce l'inserimento di visitatori
	 */
	public static class InsertListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// recupero la lista dei visitatori e la salvo in una lista
			final List<Visitor> visitors = FileUtilities.readObjects(ApplicationFile.VISITORS);
			// salvo il visitatore inserito nella view
			final Optional<Visitor> vi = visitorsView.getVisitor();
			// controllo che non ci siano errori
			if (vi.isPresent()) {
				final Visitor v = vi.get();
				if (v.getName().length() < 2 || v.getSurname().length() < 2 || !checkPrisoner(v))
					visitorsView.displayErrorMessage("Devi inserire un nome, un cognome e un prigioniero esistente");
				else {
					// inserisco il visitatore nella lista
					Objects.requireNonNull(visitors).add(vi.get());
					visitorsView.displayErrorMessage("Visitatore inserito");
				}
			} else {
				visitorsView.displayErrorMessage("Id prigioniero non valido");
			}
			// salvo la lista aggiornata
			FileUtilities.writeObjects("res/Visitors.txt", visitors);
		}

	}

	/**
	 * controlla se l'id del prigioniero inserita è corretta
	 * 
	 * @param v
	 *            visitatore
	 * @return true se l'id è corretto
	 */
	static boolean checkPrisoner(Visitor v) {
		final List<Prisoner> list = FileUtilities.readObjects(ApplicationFile.CURRENT_PRISONERS);
		boolean found = false;
		// ciclo tutti i prigionieri
		for (Prisoner p : list) {
			// se gli id conicidono restituisco true
			if (p.equals(v.getVisitedPrisoner())) {
				found = true;
			}
		}
		return found;
	}
	
}
