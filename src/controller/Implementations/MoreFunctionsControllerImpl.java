package controller.Implementations;

import controller.Interfaces.MoreFunctionsController;
import model.Interfaces.CrimeType;
import model.Interfaces.Prisoner;
import view.Components.BarChart_AWT;
import view.Components.PieChart_AWT;
import view.Interfaces.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * controller della more functions view
 */
public class MoreFunctionsControllerImpl implements MoreFunctionsController {

	private MoreFunctionsView moreFunctionsView;

	/**
	 * costruttore
	 * 
	 * @param moreFunctionsView
	 *            la view
	 */
	MoreFunctionsControllerImpl(MoreFunctionsView moreFunctionsView) {
		this.moreFunctionsView = moreFunctionsView;
		moreFunctionsView.addBackListener(new BackListener());
		moreFunctionsView.addAddMovementListener(new AddMovementListener());
		moreFunctionsView.addBalanceListener(new BalanceListener());
		moreFunctionsView.addChart1Listener(new Chart1Listener());
		moreFunctionsView.addChart2Listener(new Chart2Listener());
		moreFunctionsView.addAddVisitorsListener(new AddVisitorsListener());
		moreFunctionsView.addViewVisitorsListener(new ViewVisitorsListener());
		moreFunctionsView.addViewCellsListener(new ViewCellsListener());
	}

	/**
	 * listener che fa tornare alla pagina precedente
	 * 
	 * @author Utente
	 *
	 */
	public class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			moreFunctionsView.dispose();
			new MainControllerImpl(new MainView(moreFunctionsView.getRank()));
		}
	}

	/**
	 * listener che apre la add movement view
	 */
	public class AddMovementListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			moreFunctionsView.dispose();
			new AddMovementControllerImpl(new AddMovementView(moreFunctionsView.getRank()));
		}
	}

	/**
	 * listener che apre la balance view
	 */
	public class BalanceListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			moreFunctionsView.dispose();
			new BalanceControllerImpl(new BalanceView(moreFunctionsView.getRank()));
		}
	}

	public void createChart1() {
		// creo una mappa contenente gli anni e il numero dei prigioneri in
		// quell anno
		final Map<Integer, Integer> map = new TreeMap<>();
		// anno in cui ha aperto la prigione
		final int OPENING = 2017;
		// salvo la lista di prigionieri
		final List<Prisoner> list = FileUtilities.readObjects(ApplicationFile.PRISONERS);
		// recupero l'anno massimo in cui un prigioniero è imprigionato
		int max = getMax(list);
		// ciclo tutti gli anni e modifico il numero di prigionieri
		for (int i = OPENING; i <= max; i++) {
			int num = 0;
			Objects.requireNonNull(list);
			for (Prisoner p : list) {
				Calendar calendar = Calendar.getInstance();
				Calendar calendar2 = Calendar.getInstance();
				calendar.setTime(p.getImprisonmentStartDate());
				calendar2.setTime(p.getImprisonmentEndDate());
				if (calendar.get(Calendar.YEAR) <= i && calendar2.get(Calendar.YEAR) >= i) {
					num++;
				}
			}
			map.put(i, num);
		}
		// creo il grafico
		BarChart_AWT chart = new BarChart_AWT(map, "Numero prigionieri per anno", "Numero prigionieri per anno");
		chart.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public int getMax(List<Prisoner> list) {
		int max = 0;
		for (Prisoner p : list) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(p.getImprisonmentEndDate());
			if (calendar.get(Calendar.YEAR) > max) {
				max = calendar.get(Calendar.YEAR);
			}
		}
		return max;
	}

	/**
	 * listener che si occupa di aprire il primo grafico
	 */
	public class Chart1Listener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			createChart1();
		}
	}

	public void createChart2() {
		 final List<Prisoner> list = FileUtilities.readObjects(ApplicationFile.PRISONERS);
		// creo una mappa in cui inserire come chiave il reato e come valore il
		// numero di prigionieri che l'anno commesso
		Map<String, Integer> map = new HashMap<>();
		for (CrimeType c : CrimeType.values()) {
			map.put(c.getDescription(), 0);
		}
		Objects.requireNonNull(list);
		for (Prisoner p : list) {
			for (CrimeType c : p.getCrimes()) {
				map.put(c.getDescription(), map.get(c.getDescription()) + 1);
			}
		}
		// creo il grafico
		PieChart_AWT pie = new PieChart_AWT("Percentuale crimini commessi dai reclusi attuali", map);
		pie.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	/**
	 * listener che si occupa di lanciare il secondo grafico
	 */
	public class Chart2Listener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			createChart2();
		}
	}

	/**
	 * listener che apre l'add visitors view
	 */
	public class AddVisitorsListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			moreFunctionsView.dispose();
			new AddVisitorsControllerImpl(new AddVisitorsView(moreFunctionsView.getRank()));
		}
	}

	/**
	 * listener che apre la view visitors view
	 */
	public class ViewVisitorsListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			moreFunctionsView.dispose();
			new ViewVisitorsControllerImpl(new ViewVisitorsView(moreFunctionsView.getRank()));
		}
	}

	/**
	 * listener che apre la view cells view
	 */
	public class ViewCellsListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			moreFunctionsView.dispose();
			new ViewCellsControllerImpl(new ViewCellsView(moreFunctionsView.getRank()));
		}
	}
	
}
