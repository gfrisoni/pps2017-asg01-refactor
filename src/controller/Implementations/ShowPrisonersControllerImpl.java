package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.swing.JTable;

import model.Interfaces.Prisoner;
import view.Components.PrisonManagerJFrame;
import view.Interfaces.ShowPrisonersView;
import view.Interfaces.SupervisorFunctionsView;

/**
 * controller che gestisce la show prisoners view
 */
public class ShowPrisonersControllerImpl extends PrisonManagerJFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2056633481557914162L;

	static ShowPrisonersView showPrisonersView;

	/**
	 * controller
	 * 
	 * @param showPrisonersView
	 *            la view
	 */
	ShowPrisonersControllerImpl(ShowPrisonersView showPrisonersView) {
		ShowPrisonersControllerImpl.showPrisonersView = showPrisonersView;
		ShowPrisonersControllerImpl.showPrisonersView.addBackListener(new BackListener());
		ShowPrisonersControllerImpl.showPrisonersView.addComputeListener(new ComputeListener());
	}

	/**
	 * listener che apre la view precedente
	 */
	public static class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			showPrisonersView.dispose();
			new SupervisorControllerImpl(new SupervisorFunctionsView(showPrisonersView.getRank()));
		}
	}

	/**
	 * crea una tabella contenente i prigionieri che tra le due date prese in
	 * input erano in prigione
	 */
	public static class ComputeListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// salvo le date inserite nella view
			final Date from = showPrisonersView.getFrom();
			final Date to = showPrisonersView.getTo();
			final List<Prisoner> list = FileUtilities.readObjects(ApplicationFile.PRISONERS);
			final List<Prisoner> inclusi = new ArrayList<>();
			for (Prisoner p : list) {
				// se il prigioniero è imprigionato tra le due date lo aggiungo
				// alla lista inclusi
				if (p.getImprisonmentStartDate().before(to) && p.getImprisonmentEndDate().after(from)) {
					inclusi.add(p);
				}
			}
			// creo una matrice con i prigionieri inclusi
			final String[] vet = { "id", "nome", "cognome", "giorno di nascita", "inizio prigionia", "fine prigionia" };
			final String[][] mat = new String[inclusi.size()][vet.length];
			for (int i = 0; i < inclusi.size(); i++) {
				mat[i][0] = String.valueOf(inclusi.get(i).getId());
				mat[i][1] = inclusi.get(i).getName();
				mat[i][2] = inclusi.get(i).getSurname();
				mat[i][3] = inclusi.get(i).getBirthDate().toString();
				mat[i][4] = inclusi.get(i).getImprisonmentStartDate().toString();
				mat[i][5] = inclusi.get(i).getImprisonmentEndDate().toString();
			}
			// creo la tabella passandogli i dati della matrice
			showPrisonersView.createTable(new JTable(mat, vet));
		}
	}
	
}
