package controller.Implementations;

/**
 * This enumeration describes the files handled by the application.
 */
public enum ApplicationFile {

    MOVEMENTS("res/AllMovements.txt"),
    CURRENT_PRISONERS("res/CurrentPrisoners.txt"),
    GUARDS_USER_PASS("res/GuardieUserPass.txt"),
    PRISON("res/Prison.txt"),
    PRISONERS("res/Prisoners.txt"),
    VISITORS("res/Visitors.txt");

    private final String path;

    /**
     * Creates a new application file.
     *
     * @param path
     * 		the file's path
     */
    ApplicationFile(final String path) {
        this.path = path;
    }

    /**
     * @return the path of the application file.
     */
    public String getPath() {
        return this.path;
    }

}
