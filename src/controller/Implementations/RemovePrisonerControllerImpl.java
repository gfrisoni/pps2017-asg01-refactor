package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import controller.Interfaces.RemovePrisonerController;
import model.Implementations.CellImpl;
import model.Interfaces.Cell;
import model.Interfaces.Floor;
import model.Interfaces.Prison;
import model.Interfaces.Prisoner;
import view.Interfaces.MainView;
import view.Interfaces.RemovePrisonerView;

/**
 * controller della remove prisoner view
 */
public class RemovePrisonerControllerImpl implements RemovePrisonerController {

	private static RemovePrisonerView removePrisonerView;

	/**
	 * costruttore
	 * 
	 * @param removePrisonerView
	 *            la view
	 */
    RemovePrisonerControllerImpl(RemovePrisonerView removePrisonerView) {
		RemovePrisonerControllerImpl.removePrisonerView = removePrisonerView;
		removePrisonerView.addRemoveListener(new RemoveListener());
		removePrisonerView.addBackListener(new BackListener());
	}

	/**
	 * listener che si occupa di rimuovare il prigioniero
	 */
	public class RemoveListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			removePrisoner();
		}
	}

	public void removePrisoner() {
		final List<Prisoner> currentPrisoners = FileUtilities.readObjects(ApplicationFile.CURRENT_PRISONERS);
        final Optional<Prison> prison = FileUtilities.readObject(ApplicationFile.PRISON);
        prison.ifPresent(p -> {
            boolean found = false;

            // ciclo tutti i prigionieri
            for (Prisoner pr : currentPrisoners) {
                // se l'id corrisponde elimino il prigioniero
                if (pr.getId() == removePrisonerView.getID()) {
                    found = true;

                    currentPrisoners.remove(pr);
                    removePrisonerView.displayErrorMessage("Prigioniero rimosso");
                    // recupero la lista di celle

                    final Optional<Cell> cellWithPrisoner = p.getFloors()
                            .stream()
                            .map(Floor::getCells)
                            .flatMap(List::stream)
                            .collect(Collectors.toList()).stream().filter(c -> c.getPrisoners().contains(p))
                            .findFirst();
                    cellWithPrisoner.ifPresent(c -> c.removePrisoner(pr));
                    FileUtilities.writeObject("res/Prison.txt", p);
                }
            }

            // salvo la lista aggiornata di prigionieri
            FileUtilities.writeObjects("res/CurrentPrisoners.txt", currentPrisoners);

            if (!found) {
                removePrisonerView.displayErrorMessage("Prigioniero non trovato");
            }
        });
	}

	/**
	 * listener che apre la view precedente
	 */
	public class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			removePrisonerView.dispose();
			new MainControllerImpl(new MainView(removePrisonerView.getRank()));
		}
	}

}