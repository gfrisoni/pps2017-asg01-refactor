package controller.Implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * This utility class handles file operations.
 */
public class FileUtilities {

    private static <T> T readAndPerform(String path, BiFunction<FileInputStream, ObjectInputStream, T> onSuccess, Supplier<T> onFailure) {
        try {
            final FileInputStream fi = new FileInputStream(new File(path));
            final ObjectInputStream oi = new ObjectInputStream(fi);
            final T object = onSuccess.apply(fi, oi);
            fi.close();
            oi.close();
            return object;
        } catch (IOException e) {
            return onFailure.get();
        }
    }

    private static boolean writeAndConsume(String path, Function<ObjectOutputStream, Boolean> onSuccess) {
        boolean isSuccessful;
        try {
            final FileOutputStream fo = new FileOutputStream(new File(path));
            final ObjectOutputStream oo = new ObjectOutputStream(fo);
            fo.flush();
            oo.flush();
            isSuccessful = onSuccess.apply(oo);
            fo.close();
            oo.close();
        } catch (IOException e) {
            isSuccessful = false;
        }
        return isSuccessful;
    }

    /**
     * Reads an object from the specified file.
     *
     * @param file
     *      the application file from which read
     * @param <T>
     *      the type of the object to read
     * @return the object read.
     */
    public static <T> Optional<T> readObject(ApplicationFile file) {
        return readAndPerform(file.getPath(), (fi, oi) -> {
            try {
                return Optional.of((T)oi.readObject());
            } catch (IOException | ClassNotFoundException e) {
                return Optional.empty();
            }
        }, Optional::empty);
    }

    /**
     * Reads a list of objects from the specified file.
     *
     * @param file
     *      the application file from which read
     * @param <T>
     *      the type of the object to read
     * @return the objects read.
     */
    public static <T> List<T> readObjects(ApplicationFile file) {
        return readAndPerform(file.getPath(), (fi, oi) -> {
            try {
                final List<T> list = new ArrayList<>();
                while (fi.available() > 0) {
                    list.add((T)oi.readObject());
                }
                return list;
            } catch (IOException | ClassNotFoundException e) {
                return new ArrayList<>();
            }
        }, ArrayList::new);
    }

    /**
     * Writes an object in the specified file.
     *
     * @param path
     *      the path of the file in which write
     * @param object
     *      the object to write
     * @param <T>
     *      the type of the object to write
     * @return true if the saving of the object is successful, false otherwise.
     */
    public static <T> boolean writeObject(String path, T object) {
        return writeAndConsume(path, oo -> {
            try {
                oo.writeObject(object);
                return true;
            } catch (IOException e) {
                return false;
            }
        });
    }

    /**
     * Writes a list of objects in the specified file.
     *
     * @param path
     *      the path of the file in which write
     * @param objects
     *      the objects to write
     * @param <T>
     *      the type of the objects to write
     * @return true if the saving of the objects is successful, false otherwise.
     */
    public static <T> boolean writeObjects(String path, List<T> objects) {
        return writeAndConsume(path, oo -> {
            try {
                for (T object : objects)
                    oo.writeObject(object);
                return true;
            } catch (IOException e) {
                return false;
            }
        });
    }

}
