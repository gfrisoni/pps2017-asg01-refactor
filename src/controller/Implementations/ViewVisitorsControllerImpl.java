package controller.Implementations;

import controller.Interfaces.ViewVisitorsController;
import model.Interfaces.Visitor;
import view.Interfaces.MoreFunctionsView;
import view.Interfaces.ViewVisitorsView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * controller della view visitors view
 * 
 * @author Utente
 *
 */
public class ViewVisitorsControllerImpl implements ViewVisitorsController {

	private static ViewVisitorsView viewVisitorsView;

	/**
	 * costruttore
	 * 
	 * @param viewVisitorsView
	 *            la view
	 */
    ViewVisitorsControllerImpl(ViewVisitorsView viewVisitorsView) {
		ViewVisitorsControllerImpl.viewVisitorsView = viewVisitorsView;
		viewVisitorsView.addBackListener(new BackListener());
		viewVisitorsView.createTable(getTable());
	}

	/**
	 * listener che apre la view precedente
	 */
	public class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			viewVisitorsView.dispose();
			new MoreFunctionsControllerImpl(new MoreFunctionsView(viewVisitorsView.getRank()));
		}
	}

	public JTable getTable() {
		// creo una lista in cui inserisco i visitatori
		final List<Visitor> list = FileUtilities.readObjects(ApplicationFile.VISITORS);
		// metto gli elementi della lista in una matrice
		final String[] vet = { "Nome", "Cognome", "Data di nascita", "ID prigioniero visitato" };
		final String[][] mat = new String[list.size()][vet.length];
		for (int i = 0; i < list.size(); i++) {
			mat[i][0] = list.get(i).getName();
			mat[i][1] = list.get(i).getSurname();
			mat[i][2] = list.get(i).getBirthDate().toString();
			mat[i][3] = String.valueOf(list.get(i).getVisitedPrisoner().getId());
		}
		// creo la tabella passandogli la matrice
		return new JTable(mat, vet);
	}

}
