package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import controller.Implementations.LoginControllerImpl;
import model.Implementations.GuardImpl;
import model.Implementations.FloorType;
import model.Implementations.factories.CellFactoryImpl;
import model.Implementations.factories.PrisonFactoryImpl;
import model.Implementations.factories.strategies.StandardPrisonFloorStructure;
import model.Interfaces.Cell;
import model.Interfaces.Guard;
import model.Interfaces.Prison;
import model.Interfaces.factories.CellFactory;
import model.Interfaces.factories.PrisonFactory;
import view.Interfaces.LoginView;

/**
 * The main of the application.
 */
public final class Main {

	/**
	 * Program main, this is the "root" of the application.
	 * 
	 * @param args
	 *            unused,ignore
	 */
	public static void main(final String... args) {
		// creo cartella in cui mettere i dati da salvare
		final String Dir = "res";
		new File(Dir).mkdir();
		// creo file per le guardie
		final File fg = new File("res/GuardieUserPass.txt");
		// se il file non è stato inizializzato lo faccio ora
		if (fg.length() == 0) {
			try {
				initializeGuards(fg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// leggo il file contenente le celle
		final File f = new File("res/Prison.txt");
		// se il file non è ancora stato inizializzato lo faccio ora
		if (f.length() == 0) {
			try {
				initializePrison(f);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// chiamo controller e view del login
		new LoginControllerImpl(new LoginView());
	}

	/**
	 * Initializes the prison.
	 *
	 * @param file
     *      the file in which create the prison
	 * @throws IOException
	 */
	private static void initializePrison(final File file) throws IOException {
		final PrisonFactory prisonFactory = new PrisonFactoryImpl();
		final Prison prison = prisonFactory.createPrisonWithSameFloorCellsCapacity(new StandardPrisonFloorStructure());
		final FileOutputStream fo = new FileOutputStream(file);
		final ObjectOutputStream os = new ObjectOutputStream(fo);
		os.flush();
		fo.flush();
		os.writeObject(prison);
        os.close();
	}

	/**
	 * metodo che inizializza le guardie
	 * 
	 * @param fg
	 *            file in cui creare le guardie
	 * @throws IOException
	 */
	private static void initializeGuards(final File fg) throws IOException {
		final List<Guard> guards = new ArrayList<>();
		final Guard g1 = new GuardImpl.Builder(1, "Oronzo", "Cantani", "ciao01").build();
        guards.add(g1);
		final Guard g2 = new GuardImpl.Builder(2,"Emile", "Heskey", "asdasd")
                .rank(2)
                .telephoneNumber("456789")
                .build();
        guards.add(g2);
		final Guard g3 = new GuardImpl.Builder(3, "qwerty", "Gennaro", "Alfieri")
                .rank(3)
                .telephoneNumber("0764568")
                .build();
        guards.add(g3);
		final FileOutputStream fo = new FileOutputStream(fg);
		final ObjectOutputStream os = new ObjectOutputStream(fo);
		os.flush();
		fo.flush();
		for (Guard g : guards) {
			os.writeObject(g);
		}
		os.close();
	}

}
