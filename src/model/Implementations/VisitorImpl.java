package model.Implementations;

import java.util.Date;

import model.Interfaces.Prisoner;
import model.Interfaces.Visitor;

/**
 * Implementation of {@link Visitor}.
 * 
 */
public class VisitorImpl extends PersonImpl implements Visitor {

	/**
	 * Auto-generated serial version.
	 */
	private static final long serialVersionUID = 5306827736761721189L;

	private Prisoner prisoner;
	
	/**
	 * Constructs a new visitor.
	 * 
	 * @param name
	 * 		the visitor name
	 * @param surname
	 * 		the visitor surname
	 * @param birthDate
	 * 		the visitor birth date
	 * @param prisoner
	 * 		the prisoner related to the visitor
	 */
	public VisitorImpl(final String name, final String surname, final Date birthDate, final Prisoner prisoner) {
		super(name, surname, birthDate);
		this.prisoner = prisoner;
	}

	@Override
	public Prisoner getVisitedPrisoner() {
		return this.prisoner;
	}

	@Override
	public void setVisitedPrisoner(final Prisoner prisoner) {
		this.prisoner = prisoner;
	}

	@Override
	public String toString() {
		return new StringBuilder()
				.append("VisitorImpl [name=")
				.append(this.getName())
				.append(", surname=")
				.append(this.getSurname())
				.append(", birthDate=")
				.append(this.getBirthDate().toString())
				.append(", prisonerId=")
				.append(this.prisoner.getId())
				.append("]")
				.toString();
	}

}
