package model.Implementations;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import model.Interfaces.Cell;
import model.Interfaces.Prisoner;

/**
 * Implementation of {@link Cell}.
 */
public class CellImpl implements Cell, Serializable {

	/**
	 * Auto-generated serial version.
	 */
	private static final long serialVersionUID = 9167940013424894676L;

	private final int id;
	private int capacity;
	private List<Prisoner> prisoners;

	/**
	 * Constructs a new cell.
	 * 
	 * @param id
	 * 		cell's id
	 * @param capacity
	 * 		cell's capacity
	 */
	public CellImpl(final int id, final int capacity) {
		this.id = id;
		this.capacity = capacity;
		this.prisoners = new ArrayList<>();
	}
	
	/**
	 * Constructs a new cell from an existing one.
	 * It is used for defensive copies.
	 * 
	 * @param cell
	 * 		the cell to copy
	 */
	public CellImpl(final Cell cell) {
		this.id = cell.getId();
		this.capacity = cell.getCapacity();
		this.prisoners = cell.getPrisoners();
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public int getCapacity() {
		return this.capacity;
	}

	@Override
	public void setCapacity(final int capacity) {
		this.capacity = capacity;
	}

	@Override
	public void addPrisoner(final Prisoner prisoner) {
		Objects.requireNonNull(prisoner);
		this.prisoners.add(prisoner);
	}

	@Override
	public void removePrisoner(final Prisoner prisoner) {
		Objects.requireNonNull(prisoner);
		this.prisoners.remove(prisoner);
	}

	@Override
	public List<Prisoner> getPrisoners() {
		return this.prisoners;
	}

	@Override
	public int getPrisonersCount() {
		return this.prisoners.size();
	}

	@Override
	public String toString() {
		return new StringBuilder()
				.append("CellImpl [id=")
				.append(this.id)
				.append(", capacity=")
				.append(this.capacity)
				.append(", prisoners=")
				.append(this.prisoners)
				.append("]").toString();
	}

}
