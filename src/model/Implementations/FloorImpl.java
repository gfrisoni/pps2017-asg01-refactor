package model.Implementations;

import model.Interfaces.Cell;
import model.Interfaces.Floor;

import java.io.Serializable;
import java.util.List;

public class FloorImpl implements Floor, Serializable {

    /**
     * Auto-generated serial version.
     */
    private static final long serialVersionUID = 9167940013424894676L;

    private final FloorType type;
    private final List<Cell> cells;

    public FloorImpl(final FloorType type, final List<Cell> cells) {
        this.type = type;
        this.cells = cells;
    }

    @Override
    public FloorType getType() {
        return this.type;
    }

    @Override
    public List<Cell> getCells() {
        return this.cells;
    }

    @Override
    public String toString() {
        return "FloorImpl [type=" + type.getDescription() + ", numCells=" + cells.size() + "]";
    }

}
