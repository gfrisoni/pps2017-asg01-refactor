package model.Implementations;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import model.Interfaces.Movement;

/**
 * Implementation of {@link Movement}.
 * 
 */
public class MovementImpl implements Serializable, Movement {

	/**
	 * Auto-generated serial version.
	 */
	private static final long serialVersionUID = -8676396152502823263L;
	
	private final String description;
	private final double amount;
	private final Date date;
	private final boolean sign;

	/**
	 * Constructs a new movement.
	 * 
	 * @param description
	 * 		the movement description
	 * @param amount
	 * 		the amount involved by the movement
	 * @param sign
	 * 		the movement sign
	 */
	public MovementImpl(final String description, final double amount, final boolean sign) {
		this.description = description;
		this.amount = amount;
		this.sign = sign;
		this.date = Calendar.getInstance().getTime();
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public double getAmount() {
		return this.amount;
	}

	@Override
	public boolean isPositive() {
		return this.sign;
	}

	@Override
	public Date getDate() {
		return (Date) this.date.clone();
	}
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("MovementImpl [description=")
				.append(this.description)
				.append(", isPositive=")
				.append(this.isPositive())
				.append(", amount=")
				.append(this.amount)
				.append(", date=")
				.append(this.date.toString())
				.append("]")
				.toString();
	}
	
}
