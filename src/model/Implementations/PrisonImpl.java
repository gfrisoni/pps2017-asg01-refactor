package model.Implementations;

import model.Interfaces.Floor;
import model.Interfaces.Prison;

import java.io.Serializable;
import java.util.List;

public class PrisonImpl implements Prison, Serializable {

    /**
     * Auto-generated serial version.
     */
    private static final long serialVersionUID = 9167940013424894676L;

    private final List<Floor> floors;

    public PrisonImpl(final List<Floor> floors) {
        this.floors = floors;
    }

    @Override
    public List<Floor> getFloors() {
        return this.floors;
    }

    @Override
    public String toString() {
        return "PrisonImpl [numFloors=" + floors.size() + "]";
    }

}
