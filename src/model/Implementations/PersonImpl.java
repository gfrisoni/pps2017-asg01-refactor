package model.Implementations;

import java.io.Serializable;
import java.util.Date;

import model.Interfaces.Person;

/**
 * Implementation of {@link Person}.
 * 
 */
public abstract class PersonImpl implements Serializable, Person {

	/**
	 * Auto-generated serial version.
	 */
	private static final long serialVersionUID = -6542753884631178660L;

	private final String name;
	private final String surname;
	private final Date birthDate;

	/**
	 * Constructs a new person.
	 *
	 * @param name
	 * 		the person name
	 * @param surname
	 * 		the person surname
	 * @param birthDate
	 * 		the person birth date
	 */
	public PersonImpl(final String name, final String surname, final Date birthDate) {
		super();
		this.name = name;
		this.surname = surname;
		this.birthDate = birthDate;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getSurname() {
		return surname;
	}

	@Override
	public Date getBirthDate() {
		return (Date) birthDate.clone();
	}

	@Override
	public String toString() {
		return new StringBuilder()
				.append("PersonImpl [name=")
				.append(this.name)
				.append(", surname=")
				.append(this.surname)
				.append(", birthDate=")
				.append(this.birthDate.toString())
				.append("]")
				.toString();
	}

}
