package model.Implementations;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;

import model.Interfaces.Guard;

/**
 * Implementation of {@link Guard}.
 * 
 */
public class GuardImpl extends PersonImpl implements Guard {

	/**
	 * Auto-generated serial version.
	 */
	private static final long serialVersionUID = 3975704240795357459L;

	private final int id;
	private String password;
	private int rank;
	private String telephoneNumber;

	public static class Builder {
	    // Required parameters
		private final int id;
		private final String name;
		private final String surname;
		private final String password;

		// Optional parameters
        private Date birthDate = new GregorianCalendar(1980, 1, 1).getTime();
		private int rank = 0;
		private String telephoneNumber = "";

        public Builder(int id, String name, String surname, String password) {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.password = password;
        }

        public Builder birthDate(Date val) {
            this.birthDate = val;
            return this;
        }

        public Builder rank(int val) {
            this.rank = val;
            return this;
        }

        public Builder telephoneNumber(String val) {
            telephoneNumber = val;
            return this;
        }

        public Guard build() {
            return new GuardImpl(this);
        }
	}

    private GuardImpl(Builder builder) {
        super(builder.name, builder.surname, builder.birthDate);
        this.id = builder.id;
        this.password = builder.password;
        this.rank = builder.rank;
        this.telephoneNumber = builder.telephoneNumber;
    }
	
	@Override
	public int getId() {
		return this.id;
	}
	
	@Override
	public String getPassword() {
		return this.password;
	}
	
	@Override
	public void setPassword(final String password) {
		this.password = password;
	}

	@Override
	public String getTelephoneNumber() {
		return this.telephoneNumber;
	}

	@Override
	public void setTelephoneNumber(final String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	@Override
	public int getRank() {
		return this.rank;
	}

	@Override
	public void setRank(final int rank) {
		this.rank = rank;
	}

	@Override
	public String toString() {
		return new StringBuilder()
				.append("GuardImpl [id=")
				.append(this.id)
				.append(", rank=")
				.append(this.rank)
				.append(", telephoneNumber=")
				.append(this.telephoneNumber)
				.append("]")
				.toString();
	}

}
