package model.Implementations;

/**
 * This enumeration describes the floors inside the prison.
 *
 */
public enum FloorType {

	FIRST_FLOOR("Primo piano"),	
	SECOND_FLOOR("Secondo piano"),
	THIRD_FLOOR("Terzo piano"),
	UNDERGROUND_FLOOR("Piano sotterraneo, celle di isolamento");
	
	private final String description;

	/**
	 * Creates a new prison floor.
	 * 
	 * @param description
	 * 		prison floor description
	 */
	FloorType(final String description) {
        this.description = description;
    }

    /**
     * @return the description of the prison floor.
     */
    public String getDescription() {
        return this.description;
    }
    
}
