package model.Implementations;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import model.Implementations.factories.CellFactoryImpl;
import model.Interfaces.Cell;
import model.Interfaces.CrimeType;
import model.Interfaces.Prisoner;

/**
 * Implementation of {@link Prisoner}.
 * 
 */
public class PrisonerImpl extends PersonImpl implements Prisoner {

	/**
	 * Auto-generated serial version.
	 */
	private static final long serialVersionUID = -3204660779285410481L;

	private final int id;
	private Date imprisonmentStartDate;
	private Date imprisonmentEndDate;
	private Set<CrimeType> crimes;

	public PrisonerImpl(final int id, final String name, final String surname, final Date birthDate,
			final Date imprisonmentStartDate, final Date imprisonmentEndDate, final Set<CrimeType> crimes) {
		super(name, surname, birthDate);
		this.id = id;
		this.imprisonmentStartDate = imprisonmentStartDate;
		this.imprisonmentEndDate = imprisonmentEndDate;
		this.crimes = crimes;
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public void addCrime(final CrimeType crime) {
		crimes.add(crime);
	}

	@Override
	public Set<CrimeType> getCrimes() {
		return Collections.unmodifiableSet(this.crimes);
	}

	@Override
	public Date getImprisonmentStartDate() {
		return (Date) this.imprisonmentStartDate.clone();
	}

	@Override
	public void setImprisonmentStartDate(final Date startDate) {
		this.imprisonmentStartDate = startDate;
	}

	@Override
	public Date getImprisonmentEndDate() {
		return (Date) this.imprisonmentEndDate.clone();
	}

	@Override
	public void setImprisonmentEndDate(final Date endDate) {
		this.imprisonmentEndDate = endDate;
	}

	@Override
	public String toString() {
		return new StringBuilder()
				.append("PrisonerImpl [idPrisoner=")
				.append(this.id)
				.append(", startDate=")
				.append(this.imprisonmentStartDate.toString())
				.append(", endDate=")
				.append(this.imprisonmentEndDate.toString())
				.append(", crimes=")
				.append(this.crimes.stream().map(CrimeType::getDescription).collect(Collectors.joining(",")))
				.append("]")
				.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof PrisonerImpl && this.id == ((PrisonerImpl) obj).id;
	}
	
}
