package model.Implementations.factories;

import model.Implementations.PrisonImpl;
import model.Interfaces.Cell;
import model.Interfaces.Floor;
import model.Interfaces.Prison;
import model.Interfaces.factories.CellFactory;
import model.Interfaces.factories.PrisonFactory;
import model.Interfaces.factories.strategies.PrisonFloorStructureStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class PrisonFactoryImpl implements PrisonFactory {

    @Override
    public Prison createPrison(List<Floor> floors) {
        return new PrisonImpl(floors);
    }

    @Override
    public Prison createPrisonWithSameFloorCellsCapacity(PrisonFloorStructureStrategy structureStrategy) {
        final AtomicInteger cellCounter = new AtomicInteger(0);
        final List<Floor> floors = new ArrayList<>();
        final FloorFactoryImpl floorFactory = new FloorFactoryImpl();
        final CellFactory cellFactory = new CellFactoryImpl();
        structureStrategy.getPrisonStructure().forEach((floorType, floorStructure) -> {
            final List<Cell> floorCells = new ArrayList<>();
            IntStream.range(0, floorStructure.getNumberOfCells()).forEach(i ->
                    floorCells.add(cellFactory.createCell(cellCounter.getAndIncrement(), floorStructure.getCellsCapacity()))
            );
            floors.add(floorFactory.createFloor(floorType, floorCells));
        });
        return createPrison(floors);
    }

}
