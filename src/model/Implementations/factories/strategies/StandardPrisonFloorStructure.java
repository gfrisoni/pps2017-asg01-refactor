package model.Implementations.factories.strategies;

import model.Implementations.FloorType;
import model.Interfaces.Floor;
import model.Interfaces.factories.strategies.FloorStructure;
import model.Interfaces.factories.strategies.PrisonFloorStructureStrategy;

import java.util.HashMap;
import java.util.Map;

public class StandardPrisonFloorStructure implements PrisonFloorStructureStrategy {

    @Override
    public Map<FloorType, FloorStructure> getPrisonStructure() {
        final Map<FloorType, FloorStructure> structure = new HashMap<>();
        structure.put(FloorType.FIRST_FLOOR, new FloorStructure() {
            @Override
            public int getNumberOfCells() {
                return 20;
            }
            @Override
            public int getCellsCapacity() {
                return 4;
            }
        });
        structure.put(FloorType.SECOND_FLOOR, new FloorStructure() {
            @Override
            public int getNumberOfCells() {
                return 20;
            }
            @Override
            public int getCellsCapacity() {
                return 3;
            }
        });
        structure.put(FloorType.THIRD_FLOOR, new FloorStructure() {
            @Override
            public int getNumberOfCells() {
                return 5;
            }
            @Override
            public int getCellsCapacity() {
                return 4;
            }
        });
        structure.put(FloorType.UNDERGROUND_FLOOR, new FloorStructure() {
            @Override
            public int getNumberOfCells() {
                return 5;
            }
            @Override
            public int getCellsCapacity() {
                return 1;
            }
        });
        return structure;
    }

}
