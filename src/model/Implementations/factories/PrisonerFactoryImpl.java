package model.Implementations.factories;

import model.Implementations.PrisonerImpl;
import model.Interfaces.CrimeType;
import model.Interfaces.Prisoner;
import model.Interfaces.factories.PrisonerFactory;

import java.util.Date;
import java.util.Set;

public class PrisonerFactoryImpl implements PrisonerFactory {

    @Override
    public Prisoner createPrisoner(int id, String name, String surname, Date birthDate,
                                   Date imprisonmentStartDate,Date imprisonmentEndDate, Set<CrimeType> crimes) {
        return new PrisonerImpl(id, name, surname, birthDate, imprisonmentStartDate, imprisonmentEndDate, crimes);
    }

}
