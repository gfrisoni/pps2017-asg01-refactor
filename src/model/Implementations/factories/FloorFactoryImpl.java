package model.Implementations.factories;

import model.Implementations.FloorImpl;
import model.Implementations.FloorType;
import model.Interfaces.Cell;
import model.Interfaces.Floor;
import model.Interfaces.factories.FloorFactory;

import java.util.List;

public class FloorFactoryImpl implements FloorFactory {

    @Override
    public Floor createFloor(FloorType type, List<Cell> cells) {
        return new FloorImpl(type, cells);
    }

}
