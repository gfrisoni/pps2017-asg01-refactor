package model.Implementations.factories;

import model.Implementations.CellImpl;
import model.Interfaces.Cell;
import model.Interfaces.factories.CellFactory;

public class CellFactoryImpl implements CellFactory {

    @Override
    public Cell createCell(int id, int capacity) {
        return new CellImpl(id, capacity);
    }

    @Override
    public Cell copyCell(Cell cellToCopy) {
        return new CellImpl(cellToCopy);
    }

}
