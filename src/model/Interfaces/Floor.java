package model.Interfaces;

import model.Implementations.FloorType;

import java.util.List;

/**
 * Represents a floor inside a prison.
 */
public interface Floor {

    /**
     * @return the type of the prison floor.
     */
    FloorType getType();

    /**
     * @return the number of cells inside the prison floor.
     */
    List<Cell> getCells();

}
