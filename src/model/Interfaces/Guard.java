package model.Interfaces;

/**
 * This interface represents a prison guard.
 * 
 */
public interface Guard extends Person {

	/**
	 * @return the id of the guard
	 */
	int getId();
	
	/**
	 * @return the password of the guard
	 */
	String getPassword();
	
	/**
	 * Sets the password of the guard.
	 * 
	 * @param password
	 * 		the guard password
	 */
	void setPassword(String password);

	/**
	 * @return the telephone number of the guard
	 */
	String getTelephoneNumber();

	/**
	 * Sets the telephone number of the guard.
	 * 
	 * @param telephoneNumber
	 * 		the guard telephone
	 */
	void setTelephoneNumber(String telephoneNumber);

	/**
	 * @return the rank of the guard
	 */
	int getRank();

	/**
	 * Sets the rank of the guard.
	 * 
	 * @param rank
	 * 		the guard rank
	 */
	void setRank(int rank);

}
