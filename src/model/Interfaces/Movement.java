package model.Interfaces;

import java.util.Date;

/**
 * Represents a movement.
 *
 */
public interface Movement {

	/**
	 * @return the description of the movement
	 */
	String getDescription();

	/**
	 * @return the amount of the object moved
	 */
	double getAmount();

	/**
	 * @return the sign of the movement (true if positive, false otherwise)
	 */
	boolean isPositive();

	/**
	 * @return the date of the movement
	 */
	Date getDate();

}
