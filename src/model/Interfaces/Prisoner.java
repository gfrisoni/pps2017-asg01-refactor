package model.Interfaces;

import java.util.Date;
import java.util.Set;

/**
 * Represents a prisoner.
 * 
 */
public interface Prisoner extends Person {

	/**
	 * @return the id of the prisoner
	 */
	int getId();
	
	/**
	 * Adds a crime to the prisoner.
	 * 
	 * @param crime
	 * 		the committed crime
	 */
	void addCrime(CrimeType crime);
	
	/**
	 * @return the crimes committed by the prisoner
	 */
	Set<CrimeType> getCrimes();
	
	/**
	 * @return the imprisonment start date
	 */
	Date getImprisonmentStartDate();
	
	/**
	 * Sets the imprisonment start date.
	 * 
	 * @param startDate
	 * 		the imprisonment start date
	 */
	void setImprisonmentStartDate(Date startDate);

	/**
	 * @return the imprisonment end date
	 */
	Date getImprisonmentEndDate();
	
	/**
	 * Sets the imprisonment end date.
	 * 
	 * @param endDate
	 * 		the imprisonment end date
	 */
	void setImprisonmentEndDate(Date endDate);
	
}