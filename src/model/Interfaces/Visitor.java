package model.Interfaces;

/**
 * Represents a visitor for a prisoner.
 * 
 */
public interface Visitor extends Person {

	/**
	 * @return the visited prisoner
	 */
	Prisoner getVisitedPrisoner();

	/**
	 * Sets the visited prisoner.
	 * 
	 * @param prisoner
	 * 		the prisoner related to the visitor
	 */
	void setVisitedPrisoner(Prisoner prisoner);
	
}
