package model.Interfaces;

/**
 * This enumeration describes the possible crime types for prisoners.
 *
 */
public enum CrimeType {
	
	CRIMES_AGAINST_ANIMALS("Reati contro gli animali"),
	ASSOCIATIVE_OFFENCES("Reati associativi"),
	BLASPHEMY_SACRILEGE("Blasfemia e sacrilegio"),
	ECONOMIC_FINANCIAL_CRIMES("Reati economici e finanziari"),
	FALSE_TESTIMONY("Falsa testimonianza"),
	MILITARY_CRIMES("Reati militari"),
	CRIMES_AGAINST_HERITAGE("Reati contro il patrimonio"),
	CRIMES_AGAINST_PERSON("Reati contro la persona"),
	CRIMES_ITALIAN_LEGAL_SYSTEM("Reati nell'ordinamento italiano"),
	TAX_OFFENSES("Reati tributari"),
	DRUG_TRAFFICKING("Traffico di droga"),
	SCAM_CASES("Casi di truffa");
	
	private final String description;

	/**
	 * Creates a new crime type.
	 * 
	 * @param description
	 * 		crime type description
	 */
	CrimeType(final String description) {
        this.description = description;
    }

    /**
     * @return the description of the crime type.
     */
    public String getDescription() {
        return this.description;
    }
    
    @Override
    public String toString() {
    	return this.description;
    }
    
}
