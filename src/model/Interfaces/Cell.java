package model.Interfaces;

import java.util.List;

/**
 * This interface represents a cell of the prison.
 * 
 */
public interface Cell {

	/**
	 * @return the id of the cell
	 */
	int getId();

	/**
	 * @return the capacity of the cell
	 */
	int getCapacity();

	/**
	 * Sets the capacity of the cell.
	 * 
	 * @param capacity
	 *            cell's capacity
	 */
	void setCapacity(int capacity);

	/**
	 * Adds a new prisoner to the cell.
	 *
	 * @param prisoner
	 * 		the prisoner to add
	 */
	void addPrisoner(Prisoner prisoner);

	/**
	 * Removes a prisoner from the cell.
	 *
	 * @param prisoner
	 * 		the prisoner to remove
	 */
	void removePrisoner(Prisoner prisoner);

	/**
	 * @return the prisoners in the cell.
	 */
	List<Prisoner> getPrisoners();

	/**
	 * @return the number of prisoners in the cell
	 */
	int getPrisonersCount();
	
}
