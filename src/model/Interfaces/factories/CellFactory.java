package model.Interfaces.factories;

import model.Interfaces.Cell;

public interface CellFactory {

    /**
     * Creates a cell from its data.
     *
     * @param id
     *      the cell identifier
     * @param capacity
     *      the cell capacity
     * @return the new cell.
     */
    Cell createCell(int id, int capacity);

    /**
     * Creates a cell from another one.
     *
     * @param cellToCopy
     *      the cell to copy
     * @return the new cell.
     */
    Cell copyCell(Cell cellToCopy);

}
