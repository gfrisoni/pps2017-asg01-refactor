package model.Interfaces.factories;

import model.Implementations.FloorType;
import model.Interfaces.Cell;
import model.Interfaces.Floor;

import java.util.List;

public interface FloorFactory {

    /**
     * Creates a new prison floor.
     *
     * @param type
     *      the floor type
     * @param cells
     *      the prison cells inside the floor
     * @return the new floor.
     */
    Floor createFloor(FloorType type, List<Cell> cells);

}
