package model.Interfaces.factories;

import model.Interfaces.Floor;
import model.Interfaces.Prison;
import model.Interfaces.factories.strategies.PrisonFloorStructureStrategy;

import java.util.List;

public interface PrisonFactory {

    /**
     * Creates a prison.
     *
     * @param floors
     *      the floors of the prison
     * @return the new prison.
     */
    Prison createPrison(List<Floor> floors);

    /**
     * Creates a prison where the cells on the same floor have the same capacity.
     *
     * @param structureStrategy
     *      the structure of the prison based on its floors
     * @return the new prison.
     */
    Prison createPrisonWithSameFloorCellsCapacity(PrisonFloorStructureStrategy structureStrategy);

}
