package model.Interfaces.factories;

import model.Interfaces.CrimeType;
import model.Interfaces.Prisoner;

import java.util.Date;
import java.util.Set;

public interface PrisonerFactory {

    /**
     * Creates a prisoner.
     *
     * @param id
     *      the prisoner's identifier
     * @param name
     *      the name of the prisoner
     * @param surname
     *      the surname of the prisoner
     * @param birthDate
     *      the birth date of the prisoner
     * @param imprisonmentStartDate
     *      the imprisonment starting date of the prisoner
     * @param imprisonmentEndDate
     *      the imprisonment ending date of the prisoner
     * @param crimes
     *      the crimes related to the prisoner
     * @return the new prisoner.
     */
    Prisoner createPrisoner(int id, String name, String surname, Date birthDate,
                            Date imprisonmentStartDate, Date imprisonmentEndDate, Set<CrimeType> crimes);

}
