package model.Interfaces.factories.strategies;

public interface FloorStructure {

    /**
     * @return the number of cells inside the prison floor.
     */
    int getNumberOfCells();

    /**
     * @return the capacity of all the cells inside the prison floor.
     */
    int getCellsCapacity();

}
