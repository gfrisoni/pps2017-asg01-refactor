package model.Interfaces.factories.strategies;

import model.Implementations.FloorType;

import java.util.Map;

public interface PrisonFloorStructureStrategy {

    /**
     * @return the structure of each prison floor.
     */
    Map<FloorType, FloorStructure> getPrisonStructure();

}
