package model.Interfaces;

import java.util.List;

/**
 * Represents a prison.
 */
public interface Prison {

    /**
     * @return the floors inside the prison.
     */
    List<Floor> getFloors();

}
