package model.Interfaces;

import java.util.Date;

/**
 * Represents a person.
 * 
 */
public interface Person {

	/**
	 * @return the name of the person
	 */
	String getName();
	
	/**
	 * @return the surname of the person
	 */
	String getSurname();

	/**
	 * @return the birth date of the person
	 */
	Date getBirthDate();
	
}
