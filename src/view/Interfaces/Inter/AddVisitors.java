package view.Interfaces.Inter;

import java.util.Optional;

import controller.Implementations.AddVisitorsControllerImpl.BackListener;
import controller.Implementations.AddVisitorsControllerImpl.InsertListener;
import model.Interfaces.Visitor;

public interface AddVisitors {

	/**
	 * ritorna il visitatore inserito nella view
	 * 
	 * @return the visitor
	 */
	public Optional<Visitor> getVisitor();

	/**
	 * ritorna il rank
	 * 
	 * @return il rank
	 */
	public int getRank();

	/**
	 * mostra un messaggio
	 * 
	 * @param error
	 *            il messaggio
	 */
	public void displayErrorMessage(String error);

	/**
	 * aggiunger il backLsitener
	 * 
	 * @param backListener
	 */
	public void addBackListener(BackListener backListener);

	/**
	 * aggiunge l'insert visitor listener
	 * 
	 * @param insertListener
	 */
	public void addInsertVisitorListener(InsertListener insertListener);
	
}
