package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.AddVisitorsControllerImpl.InsertListener;
import controller.Implementations.ApplicationFile;
import controller.Implementations.FileUtilities;
import controller.Implementations.MainControllerImpl;
import controller.Implementations.AddVisitorsControllerImpl.BackListener;
import model.Implementations.VisitorImpl;
import model.Interfaces.Prisoner;
import model.Interfaces.Visitor;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.AddVisitors;

/**
 * view in cui si aggiungono visitatori
 */
public class AddVisitorsView extends PrisonManagerJFrame implements AddVisitors {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8964073612262207713L;

	int rank;
	private final String pattern = "MM/dd/yyyy";
	private final SimpleDateFormat format = new SimpleDateFormat(pattern);
	private final JButton insert = new JButton("Inserisci");
    private final JTextField name1 = new JTextField(6);
    private final JTextField surname1 = new JTextField(6);
    private final JTextField birthDate1 = new JTextField(6);
    private final JButton back = new JButton("Indietro");
    private final JTextField prisonerID1 = new JTextField(6);

	/**
	 * costruttore
	 * 
	 * @param rank
	 *            il rank della guardia che sta visualizzando il programma
	 */
	public AddVisitorsView(int rank) {
		this.rank = rank;
		this.setSize(450, 400);
		this.getContentPane().setLayout(new BorderLayout());
		PrisonManagerJPanel north = new PrisonManagerJPanel(new FlowLayout());
        JLabel title = new JLabel("Inserisci visitatore");
        north.add(title);
		this.getContentPane().add(BorderLayout.NORTH, north);
        PrisonManagerJPanel center = new PrisonManagerJPanel(new SpringLayout());
        JLabel name = new JLabel("Nome : ");
        center.add(name);
		center.add(name1);
        JLabel surname = new JLabel("Cognome :");
        center.add(surname);
		center.add(surname1);
        JLabel birthDate = new JLabel("Data di nascita (mm/gg/aaaa) : ");
        center.add(birthDate);
		center.add(birthDate1);
		birthDate1.setText("01/01/2017");
        JLabel prisonerID = new JLabel("Id prigioniero incontrato  : ");
        center.add(prisonerID);
		center.add(prisonerID1);
		prisonerID1.setText("0");
		SpringUtilities.makeCompactGrid(center, 4, 2, // rows, cols
				6, 6, // initX, initY
				6, 6); // xPad, yPad
		this.getContentPane().add(BorderLayout.CENTER, center);
		PrisonManagerJPanel south = new PrisonManagerJPanel(new FlowLayout());
		south.add(insert);
		south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setVisible(true);
	}

	public Optional<Visitor> getVisitor() {
		Optional<Visitor> v = Optional.empty();
		try {
			final Date date = format.parse(birthDate1.getText());
			v = Optional.of(new VisitorImpl(
			        name1.getText(),
                    surname1.getText(),
                    date,
                    FileUtilities.<Prisoner>readObjects(ApplicationFile.PRISONERS)
                            .stream()
                            .filter(p -> p.getId() == Integer.valueOf(prisonerID1.getText()))
                            .findFirst()
                            .get()
            ));
		} catch (ParseException | NumberFormatException e) {
			e.printStackTrace();
		}
		return v;
	}

	public int getRank() {
		return this.rank;
	}

	public void displayErrorMessage(String error) {
		JOptionPane.showMessageDialog(this, error);
	}

	public void addBackListener(BackListener backListener) {
		back.addActionListener(backListener);
	}

	public void addInsertVisitorListener(InsertListener insertListener) {
		insert.addActionListener(insertListener);
	}

}
